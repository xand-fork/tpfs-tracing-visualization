# trace-data-uploader

Trace data uploader is a CLI for upload trace logs into Jaeger.

## Example

Run this [example](./opentelemetry-json/examples/export-to-jsonl.rs) to generate a trace log file:
```bash
cargo run --example export-to-jsonl
```

Start a local jager instance with:
```bash
docker run -d -p6831:6831/udp -p6832:6832/udp -p16686:16686 jaegertracing/all-in-one:latest
```

And then run this [example](./trace-data-uploader/examples/upload-to-jaeger.rs) upload your trace data file with:
```bash
cargo run --example upload-to-jaeger
```
