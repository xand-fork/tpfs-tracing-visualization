use std::error::Error;
use structopt::StructOpt;
use trace_data_uploader::cli::{run, Args};

#[tokio::main]
async fn main() -> Result<(), Box<dyn Error>> {
    run(Args::from_args()).await.map_err(|e| e.into())
}
