use crate::input_data::InputData;
use crate::uploader;
use std::path::PathBuf;
use structopt::StructOpt;

#[derive(Debug, StructOpt)]
#[structopt(
    name = "trace-data-uploader",
    about = "Uploads an open telemetry trace data log file into a Jaeger webservice."
)]
pub struct Args {
    #[structopt(short, long, default_value = "127.0.0.1:6831")]
    pub server_socket_addr: String,
    /// Input file
    #[structopt(parse(from_os_str))]
    pub trace_log_file_path: PathBuf,
}

pub mod error {
    use crate::{input_data, uploader};

    #[derive(thiserror::Error, Debug)]
    pub enum Error {
        #[error(transparent)]
        Input(#[from] input_data::Error),
        #[error(transparent)]
        Upload(#[from] uploader::JaegerError),
    }
}

pub async fn run(args: Args) -> Result<(), error::Error> {
    println!("Using command line: {:?}\n", &args);

    let input_data: InputData = InputData::from_file(args.trace_log_file_path)?;
    let uploader = uploader::Jaeger::new(args.server_socket_addr)?;
    uploader.upload(input_data.stream()).await?;

    println!("Upload complete.");

    Ok(())
}
