.PHONY: integ-tests

VERSION_FILE=VERSION.txt
VERSION := $(shell cat $(VERSION_FILE))

export JAEGER_DOCKER_IMAGE := jaegertracing/all-in-one:1.29

integ-tests:
	docker pull ${JAEGER_DOCKER_IMAGE}
	cargo test --package integ-tests


# Intended for manual usage of Jaeger, NOT for use with integ_tests.
start:
	docker run -d --name jaeger_visualizer -p6831:6831/udp -p6832:6832/udp -p16686:16686 ${JAEGER_DOCKER_IMAGE}

stop:
	docker rm -f jaeger_visualizer


