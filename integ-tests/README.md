# Integration Tests

This project contains integration tests related to tracing.

## Executing integation tests

Use the Makefile target `integ-tests` to run integration tests. The integration tests create/start/stop/remove 
a Jaeger container during a run. The Makefile ensures the Jaeger image gets pulled before integ tests start,
as they will fail to start the container in CI otherwise. It also saves the Jaeger version to an environment var
that gets picked up and used during testing.

```bash
make integ_tests
```

## Updating Jaeger container version

If the Jaeger container version needs updating then update it in the root Makefile. 
