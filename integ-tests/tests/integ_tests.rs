#![allow(clippy::enum_variant_names)]

#[cfg(test)]
mod tests {
    mod assertions;
    mod docker;
    mod jaeger_container;
    mod opentelemetry_tracer;

    use assertions::{ChunkAssertion, JaegerClient, SpanAssertion};
    use jaeger_container::JaegerContainer;
    use opentelemetry_tracer::OpenTelemetryTracer;
    use std::time::Duration;
    use tempfile::NamedTempFile;

    #[tokio::test]
    async fn opentelemetry_exporter_is_compatible_with_jaeger() {
        // Arrange
        // Set up empty temp log file
        let temp_log_file = NamedTempFile::new().unwrap();
        let file = temp_log_file.reopen().unwrap();
        println!("Writing trace data to temp file: {:?}", &temp_log_file);

        // Act: Create trace log and upload metrics to Jaeger
        println!("Running test code to generate opentelemetry trace log file");
        let global_tracer = OpenTelemetryTracer::new(file);
        test_function();
        drop(global_tracer);

        println!("Creating jaeger container...");
        let container = JaegerContainer::new().await.unwrap();
        std::thread::sleep(Duration::from_secs(5));
        println!("Jaeger container listening at {}", container.logging_socker_addr());

        println!("Uploading traces to jaeger");
        let cli_args = trace_data_uploader::cli::Args {
            trace_log_file_path: temp_log_file.path().to_path_buf(),
            server_socket_addr: container.logging_socker_addr(),
        };
        trace_data_uploader::cli::run(cli_args).await.unwrap();

        // Assert
        let uri = format!("http://{}", container.query_socker_addr());
        println!("Querying jaeger for traces at uri: {}", uri);
        let mut jaeger = JaegerClient::new(uri).await;
        let mut span_response_chunks = jaeger.traces_for_service_name("unknown_service").await;
        let first_chunk = span_response_chunks.swap_remove(0);

        // Verify span exists for test function called above
        let mut spans = first_chunk.spans_by_function_name(stringify!(test_function));
        assert_eq!(1, spans.len(), "Expected 1 span; Found {} spans", spans.len());

        // Verify span contains info level log message with a custom field
        let span = spans.swap_remove(0);
        assert!(
            span.contains_log_with_fields(vec![("something", "useful"), ("level", "INFO")]),
            "Unable to find matching log"
        );
    }

    #[tracing::instrument]
    fn test_function() {
        tracing::info!(something = "useful");
    }
}
