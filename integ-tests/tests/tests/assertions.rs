use futures::StreamExt;
use jaeger_proto::jaeger_api::query_service_client::QueryServiceClient;
use jaeger_proto::jaeger_api::{FindTracesRequest, KeyValue, Log, Span, SpansResponseChunk, TraceQueryParameters};
use std::collections::HashMap;
use tonic::transport::Channel;

pub trait ChunkAssertion {
    fn spans_by_function_name(&self, operation_name: &str) -> Vec<&Span>;
}

impl ChunkAssertion for SpansResponseChunk {
    fn spans_by_function_name(&self, operation_name: &str) -> Vec<&Span> {
        self.spans
            .iter()
            .filter(|s| s.operation_name == operation_name)
            .collect()
    }
}

pub trait SpanAssertion {
    fn contains_log_with_fields(&self, expected_fields: Vec<(&str, &str)>) -> bool;
}

fn log_contains_field(actual_fields: &[KeyValue], expected_fields: &(&str, &str)) -> bool {
    actual_fields
        .iter()
        .any(|kv| kv.key == expected_fields.0 && kv.v_str == expected_fields.1)
}

fn log_contains_all_fields(log: &Log, expected_fields: &[(&str, &str)]) -> bool {
    expected_fields.iter().all(|ef| log_contains_field(&log.fields, ef))
}

impl SpanAssertion for Span {
    fn contains_log_with_fields(&self, fields: Vec<(&str, &str)>) -> bool {
        self.logs.iter().any(|l| log_contains_all_fields(l, &fields))
    }
}

pub struct JaegerClient {
    client: QueryServiceClient<Channel>,
}

impl JaegerClient {
    pub async fn new(jaeger_uri: String) -> Self {
        Self {
            client: QueryServiceClient::connect(jaeger_uri).await.unwrap(),
        }
    }

    pub async fn traces_for_service_name(&mut self, service_name: &str) -> Vec<SpansResponseChunk> {
        let find_request = FindTracesRequest {
            query: Some(TraceQueryParameters {
                duration_max: None,
                duration_min: None,
                operation_name: "".to_string(),
                search_depth: 999,
                service_name: service_name.to_string(),
                start_time_max: None,
                start_time_min: None,
                tags: HashMap::default(),
            }),
        };
        let stream = self.client.find_traces(find_request).await.unwrap().into_inner();
        let span_results: Vec<_> = stream.collect().await;
        span_results.into_iter().map(|r| r.unwrap()).collect()
    }
}
