use bollard::models::PortBinding;
use std::env::VarError;
use thiserror::Error;

pub type Result<T, E = Error> = std::result::Result<T, E>;

#[derive(Debug, Error)]
pub enum Error {
    #[error(transparent)]
    DockerClientError(#[from] crate::tests::docker::error::Error),
    #[error(transparent)]
    ContainerError(#[from] bollard::errors::Error),
    #[error(transparent)]
    VarError(#[from] VarError),
    #[error("Addr or Port not set in Bollard PortBinding")]
    UnexpectedPortBinding(PortBinding),
}
