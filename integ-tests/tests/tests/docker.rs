use bollard::Docker;

pub mod error {
    use thiserror::Error;

    #[derive(Debug, Error)]
    pub enum Error {
        #[error(transparent)]
        Docker(#[from] bollard::errors::Error),
    }
}

pub async fn create_docker_connection() -> Result<Docker, error::Error> {
    // First try to connect via TCP 2375
    if let Ok(docker) = Docker::connect_with_http_defaults() {
        if let Ok(_info) = docker.info().await {
            return Ok(docker);
        }
    }

    match Docker::connect_with_socket_defaults() {
        Ok(docker) => {
            if let Ok(_info) = docker.info().await {
                return Ok(docker);
            }
        }
        Err(err) => return Err(err.into()),
    }
    panic!("")
}
