use std::fs::File;
use tracing_subscriber::layer::SubscriberExt;
use tracing_subscriber::util::SubscriberInitExt;

pub struct OpenTelemetryTracer {}

impl OpenTelemetryTracer {
    pub fn new(log_file: File) -> Self {
        let json_exporter = opentelemetry_json::exporter::json::Exporter::new(log_file);
        let tracer = opentelemetry_json::pipeline::Builder::default()
            .add_simple_exporter(json_exporter)
            .install();
        let json_file_layer = tracing_opentelemetry::layer().with_tracer(tracer);
        tracing_subscriber::Registry::default()
            .with(json_file_layer)
            .try_init()
            .expect("Failed initializing global subscriber");
        OpenTelemetryTracer {}
    }
}

impl Drop for OpenTelemetryTracer {
    fn drop(&mut self) {
        opentelemetry::global::shutdown_tracer_provider();
    }
}
