use super::docker::create_docker_connection;
use bollard::container::{Config, InspectContainerOptions};
use bollard::models::{HostConfig, PortBinding};
use std::collections::HashMap;
use std::env;
use std::time::Duration;

pub use error::{Error, Result};

mod error;

#[derive(Debug)]
pub struct JaegerContainer {
    container_id: String,
    logging_socket_addr: String,
    query_socket_addr: String,
}

impl JaegerContainer {
    pub async fn new() -> Result<Self, error::Error> {
        let mut query_service_port_bindings = HashMap::new();
        query_service_port_bindings.insert(
            "16685/tcp".to_string(),
            Some(vec![PortBinding {
                host_ip: Some("0.0.0.0".to_string()),
                host_port: Some("16685".to_string()),
            }]),
        );

        let mut publish_ports = HashMap::new();
        publish_ports.insert("16685".to_string(), HashMap::new());

        let raw_container_version = env::var("JAEGER_DOCKER_IMAGE")?;
        println!("Using jaeger image {}", raw_container_version);

        let container_config = Config {
            image: Some(raw_container_version),
            host_config: Some(HostConfig {
                auto_remove: Some(true),
                publish_all_ports: Some(true),
                port_bindings: Some(query_service_port_bindings),
                ..Default::default()
            }),
            exposed_ports: Some(publish_ports),
            ..Default::default()
        };

        println!("Container configuration:\n{:?}", &container_config);
        let docker = create_docker_connection().await?;
        let container = docker.create_container::<String, _>(None, container_config).await?;
        docker.start_container::<String>(&container.id, None).await?;

        // Get upload port that jaeger is running on
        let inspected_container = docker
            .inspect_container(&container.id, Some(InspectContainerOptions { size: false }))
            .await?;

        let logging_port_binding = inspected_container
            .network_settings
            .clone()
            .and_then(|ns| ns.ports)
            .and_then(|p| p.get("6831/udp").cloned().unwrap_or(None))
            .and_then(|vec_port_binding| vec_port_binding.get(0).cloned())
            .expect("Could not get container port binding");
        let logging_socket_addr = Self::try_convert_portbinding(logging_port_binding)?;

        let query_port_binding = inspected_container
            .network_settings
            .and_then(|ns| ns.ports)
            .and_then(|p| p.get("16685/tcp").cloned().unwrap_or(None))
            .and_then(|vec_port_binding| vec_port_binding.get(0).cloned())
            .expect("Could not get container port binding");
        let query_socket_addr = Self::try_convert_portbinding(query_port_binding)?;

        Ok(Self {
            container_id: container.id,
            logging_socket_addr,
            query_socket_addr,
        })
    }

    fn try_convert_portbinding(port_binding: PortBinding) -> Result<String> {
        if let PortBinding {
            host_ip: Some(host_ip),
            host_port: Some(host_port),
        } = port_binding.clone()
        {
            return Ok(format!("{}:{}", host_ip, host_port));
        }

        Err(error::Error::UnexpectedPortBinding(port_binding))
    }

    pub fn logging_socker_addr(&self) -> String {
        self.logging_socket_addr.clone()
    }

    pub fn query_socker_addr(&self) -> String {
        self.query_socket_addr.clone()
    }
}

impl Drop for JaegerContainer {
    fn drop(&mut self) {
        let container_id = self.container_id.clone();
        let (sender, receiver) = std::sync::mpsc::channel();

        std::thread::spawn(move || {
            let new_rt = tokio::runtime::Runtime::new().unwrap();
            let tokio_handle = new_rt.handle();

            tokio_handle.block_on(async {
                let docker = create_docker_connection().await.unwrap();
                let stop_res = docker.stop_container(&container_id, None).await;
                sender.send(stop_res).unwrap();
            });
        });
        let timeout = Duration::from_secs(10);
        let stop_res = receiver.recv_timeout(timeout);
        match stop_res {
            Ok(_) => {
                println!("Stopped container: {:?}", &self.container_id)
            }
            Err(err) => {
                println!("Error while stopping trustee container: {:?}", err)
            }
        }
    }
}
