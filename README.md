# Tracing Visualization

This repo contains code necessary to visualize Open Telemetry trace logs in Jaeger

## Crates

- **[trace-data-uploader](./trace-data-uploader)** - Given a file(s) where each line is a json-serialized [`SpanData`][SpanData], this CLI uploads the span data to a backend for visualization (e.g. [Jaeger UI][jaeger]) 
- **[jaeger-proto](./jaeger-proto)** - Rust Tonic GRPC client for Jaeger 
- **[integ-tests](./integ-tests)** - Integration tests for trace-data-uploader and open telemetry json exporter

[SpanData]: https://docs.rs/opentelemetry/0.16.0/opentelemetry/sdk/export/trace/struct.SpanData.html
[tracing]: https://docs.rs/tracing/0.1.29/tracing/
[opentelemetry-rust]: https://docs.rs/opentelemetry/0.16.0/opentelemetry/
[jaeger]: https://github.com/jaegertracing/jaeger-ui


## Integration tests

Integration tests live in the `integ-tests` project. The tests depend on an environment variable for the Jaeger
docker image version. Because of this, please use the Makefile to run integration tests:
```bash
make integ-tests
```

## Versioning
Only `trace-data-uploader` is published and need version updates. Each project is versioned
separately in their corresponding `Cargo.toml` files. To bump versions, manually update the `Cargo.toml`. The beta
publishing jobs automatically suffix versions with a `beta` tag, so no need to bump version manually with a beta tag. 

## How to load and visualize trace logs
Jaeger is used to visualize client trace logs. Jaeger is a dockerized web-service that inputs open telemetry data
via it's APIs, and then presents them visually with a corresponding website. Trace logs must be captured using the
[opentelemetry-json](./opentelemetry-json) exporter. This will put the trace logs into the correct format for Jaeger.
Then the logs can be uploaded into Jaeger by the `trace-data-uploader` CLI tool in this workspace.

1. Obtain a trace log.

2. Start Jaeger locally:
    ```bash
    make start
    ```

4. Upload trace logs:
    ```bash
    cargo run <absolute_path_to_trace_log_file>
    ```

5. Open local Jaeger webpage in a browser: http://localhost:16686

6. Use the UI to search and visualize the logs
 
7. When done, stop and remove the container, which also clears the uploaded logs:
    ```bash
    make stop
    ```
