#![forbid(unsafe_code)]

pub mod jaeger_api {
    // The string specified here must match the proto package name
    include!(concat!(env!("OUT_DIR"), "/jaeger.api_v2.rs"));
}
